﻿/**
* This class Implemented by Abdul Hannan Ayubi & Furkan Taşkın
* This class is for the Robot type objects..
* Class implementation purpose is to use a virtual classes of this function...
* Date : 13/01/2021
*/

#include<iostream>
#include"RobotInterface.h"
#include"PioneerRobotInterface.h"

PioneerRobotInterface::PioneerRobotInterface() {
	this->RobotAPI = new PioneerRobotAPI;
	std::memset(ranges, 0, sizeof(ranges));
	if (RobotAPI->connect()) {
		cout << "The robot has been successfully connected!\n";
		this->setState(1);
	}
	else {
		cout << "Failed to connect to robot!\n";
		this->setState(0);
	}

}

PioneerRobotInterface::~PioneerRobotInterface() {
	delete RobotAPI;
}


void PioneerRobotInterface::turnLeft() {
	if (!this->getState()) {
		cout << "Failed to connect to robot!\n";
		return;
	}
	this->RobotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
	print();
}

void PioneerRobotInterface::turnRight() {
	if (!this->getState()) {
		cout << "Failed to connect to robot!\n";
		return;
	}
	this->RobotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
	print();
}

void PioneerRobotInterface::forward(float speed) {
	if (!this->getState()) {
		cout << "Failed to connect to robot!\n";
		return;
	}
	this->RobotAPI->moveRobot(speed);
	print();
}

void PioneerRobotInterface::print() {
	cout << "My Position is : (" << this->RobotAPI->getX() << "," << this->RobotAPI->getY() << "," << this->RobotAPI->getTh() << ")" << endl;
}

void PioneerRobotInterface::backword(float speed) {
	speed *= -1;
	if (!this->getState()) {
		cout << "Failed to connect to robot!\n";
		return;
	}
	this->RobotAPI->moveRobot(speed);
	print();
}

void PioneerRobotInterface::setPose(Pose *pose) {
	pose = new Pose;
	pose->setPose(RobotAPI->getX(), RobotAPI->getY(), RobotAPI->getTh());
	RobotInterface::setPosition(pose);
}

Pose PioneerRobotInterface::getPose() {
	Pose *pose = new Pose;
	pose->setPose(RobotAPI->getX(), RobotAPI->getY(), RobotAPI->getTh());
	RobotInterface::setPosition(pose);
	Pose temp = *RobotInterface::getPosition();
	return temp;
}

void PioneerRobotInterface::stopTurn() {
	if (!this->getState()) {
		cout << "Failed to connect to robot!\n";
		return;
	}
	this->RobotAPI->stopRobot();
	print();
}

void PioneerRobotInterface::stopMove() {
	if (!this->getState()) {
		cout << "Failed to connect to robot!\n";
		return;
	}
	this->RobotAPI->stopRobot();
	print();
}

void PioneerRobotInterface::updateSensors(RangeSensor* rangeSensor) {
	// Burada bir tane RangeSensor nesnesi alınacak ve gelen sensor tipine göre update Sensorler çağırılacak...
	// analamadım ne yapmalıyım...
	this->setRangeSensor(rangeSensor);
	this->getRangeSensor()->updateSensor(ranges);
}

void PioneerRobotInterface::setState(int status) {
	this->RobotInterface::setState(status);
}

int PioneerRobotInterface::getState() {
	return this->RobotInterface::getState();
}
