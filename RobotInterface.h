﻿/**
* This class Implemented by Abdul Hannan Ayubi & Furkan Taşkın
* This class is for Interface of the Robot type objects and this class is a pure virtual class
* Date : 13/01/2021
*/

#ifndef _ROBOTINTERFACE_
#define _ROBOTINTERFACE_
#include "Pose.h"
#include "RangeSensor.h"
class RobotInterface {
private:
	Pose* position = new Pose;
	RangeSensor* rangeSensor;
	int state;
public:
	RobotInterface() :position(NULL), state(0) {};
	RobotInterface(Pose* pose, int state) { this->position = pose; this->state = state; }
	virtual void turnLeft() = 0;
	virtual void turnRight() = 0;
	virtual void forward(float) = 0;
	virtual void print() = 0;
	virtual void backword(float) = 0;
	virtual void setPose(Pose*) = 0;
	virtual Pose getPose() = 0;
	virtual void stopTurn() = 0;
	virtual void stopMove() = 0;
	virtual void updateSensors(RangeSensor*) = 0;
	void setState(int states) { this->state = states; }
	int getState() { return this->state; };
	void setPosition(Pose* pose) { this->position = pose; }
	Pose* getPosition() { return this->position; }
	RangeSensor* getRangeSensor() { return this->rangeSensor; }
	void setRangeSensor(RangeSensor* rangeSensor) { this->rangeSensor = rangeSensor; }
};

#endif // !_ROBOTINTERFACE_

