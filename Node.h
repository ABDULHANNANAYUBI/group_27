/**
 * This class was written by Furkan Taskin.
 * 07.01.2021
 */

#ifndef NODE_H
#define NODE_H
#include "Pose.h"
#include "iostream"

using namespace std;

struct Node {
	Pose pose;
	Node* next;
	Node() {
		this->next = NULL;
	}
	Node(Pose const& pose) {
		this->pose = pose;
		this->next = NULL;
	}
};
#endif // !NODE_H
