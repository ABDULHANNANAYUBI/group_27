﻿/**
 * This class was written by Furkan Taskin.
 * 07.01.2021
 */

#include "Path.h"
#include "Node.h"
#include "Pose.h"
#include <iostream>
#include <string>

using namespace std;

Path::Path() : tail(NULL), head(NULL), number(0) {}

void Path::addPos(Pose p) {
	Node* node = new Node();
	node->pose = p;
	node->next = NULL;

	if (this->head == NULL) {
		// Case 1: First node
		this->head = this->tail = node;
	}
	else {
		// Case 2: Append to the end
		this->tail->next = node;
		this->tail = node;
	}
	this->number++;
}

void Path::print() {
	Node* temp = head;
	int i = 1;
	while (temp != NULL) {
		cout << i << "-" << temp->pose << endl;
		i++, temp = temp->next;
	}
	cout << endl;
}

Pose Path::operator[](int a) {
	Node* temp = head;
	if (a >= 0 && a <= number) {
		for (int i = 0; i < a; i++) {
			temp = temp->next;
		}
		return temp->pose;
	}
	else {
		Pose* temp = new Pose();
		temp->setPose(a, a, a);
		cout << "Wrong Input" << endl;
		return *temp;
	}
}

Pose Path::getPos(int a) {
	if (a >= 0 && a <= number) {
		Node* temp = head;
		for (int i = 0; i < a; i++) {
			temp = temp->next;
		}
		return temp->pose;
	}
	else {
		Pose* temp = new Pose();
		temp->setPose(a, a, a);
		cout << "Wrong Input" << endl;
		return *temp;
	}
}

bool Path::removePos(int a){

	if (a >= 0 && a <= number) {
		Node* p = head, * q = NULL, * r = NULL;
		for (int i = 0; i < a; i++) {
			r = q;
			q = p;
			p = p->next;
		}
		if (q == NULL) {
			this->head = this->head->next;
			if (this->head == NULL) this->tail = NULL;
		}
		else if (p == this->tail) {
			q->next = p;
			q->next = NULL;
		}
		else {
			q->next = p->next;
		} // end-else

		delete p;
		this->number--;
		return true;
	}
	else {
		return false;
	}
}

bool Path::insertPos(int a, Pose pose) {
	if (a > 0 && a <= number) {
		Node* node = new Node();
		Node* q = NULL, * p = this->head;
		node->pose = pose;
		node->next = NULL;
		for (int i = 0; i < a; i++) {
			q = p;
			p = p->next;
		}
		if (this->head == NULL) {
			this->head = this->tail = node;
		}
		else if (q == NULL) {
			node->next = p;
			p = node;
			head = p;
		}
		else {
			node->next = q->next;
			q->next = node;
		}
		return true;
	}
	else {
		return false;
	}
}

ostream& operator<<(ostream& out, Path& p) {
	Node* temp = p.head;
	while (temp != NULL) {
		out << temp->pose << endl;
		temp = temp->next;
	}
	cout << endl;
	return out;
}

istream& operator>>(istream& in, Pose* p) {
	p = new Pose;
	float TempX, TempY, TempTh;
	in >> TempX >> TempY >> TempTh;
	p->setPose(TempX, TempY, TempTh);
	return in;
}

Path::~Path() { 
	Node* temp = head;

	while (head != NULL) {
		temp = head;
		head = head->next;
		delete temp;
	}
}

string Path::getPoseAsString() {
	Node* temp = head;
	string tempStr = "";
	int i = 1;
	while (temp != NULL) {
		tempStr += to_string(i);
		tempStr += " - (";
		tempStr += to_string(temp->pose.getX());
		tempStr += ", ";
		tempStr += to_string(temp->pose.getY());
		tempStr += ", ";
		tempStr += to_string(temp->pose.getTh());
		tempStr += ")   ";

		i++;
		temp = temp->next;
	}
	return tempStr;
}
