/**
 * This class was written by Abdul Hannan Ayubi.
 * Tarih : 05/10/2020
 */
#include"RobotOperator.h"
#include<iostream>
#include<iomanip>

using namespace std;
RobotOperator::RobotOperator() {
	this->name = this->surname = "";
	this->accessCode = 0;
	this->accessState = false;
}

/// <RobotOperator>
/// Default constructor of the class
/// </RobotOperator>
/// <@param name="name"></@param>
/// <@param name="surname"></@param>
RobotOperator::RobotOperator(string name, string surname) {
	this->name = name;
	this->surname = surname;
	this->accessState = false;
}
/// <RobotOperator>
/// Parametrized Constructor of the class
/// </RobotOperator>
/// <@param name="name"></@param>
/// <@param name="surname"></@param>
/// <@param name="accessCode"></@param>
RobotOperator::RobotOperator(string name, string surname, int accessCode):accessState(false) {
	this->name = name;
	this->surname = surname;
	this->accessCode = encrypt(accessCode);
}
/// <RobotOperator>
/// RobotOperator taking the code and if the the code is true than the accesscode is true
/// </RobotOperator>
/// <@param name="accessCode"></@param>
RobotOperator::RobotOperator(unsigned int accessCode): accessState(false) {
	this->accessCode = encrypt(accessCode);
}
/// <encryptCode>
/// This function is the encrypt the given code from the inherite class
/// </encryptCode>
/// <param name="code"></param>
/// <returns code></returns code>
int RobotOperator::encryptCode(int code) {
	return encrypt(code);
}
/// <DecryptCode>
/// This function is the decrypt the given code and take from the inherite class
/// </DecryptCode>
/// <@param name="code"></@param>
/// <returns int></returns int>
int RobotOperator::decryptCode(int code) {
	return decrypt(code);
}
/// <cheackAccessCode>
/// if the entered code is the accessCode than accessCode means True
/// </cheackAccessCode>
/// <@param name="code"></@param>
/// <returns int></returns int>
bool RobotOperator::checkAccessCode(int code) {
	if (code == this->accessCode) {
		this->accessState = true;
		return true;
	}
	else {
		return false;
	}
	
}

/// <print>
/// this function is the for printing the information of the accessed person.
/// </print>
void RobotOperator::print() {
	cout << "Name" << setw(20) << "Surname" << setw(20) << "AccessCode " << setw(20)  << "Access Status" << "\n"
		<< "-------------------------------------------------------------------------------------------\n" <<
		name << setw(18) << surname << setw(18) << accessCode << setw(15) << accessState << endl;
}
