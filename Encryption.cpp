/**
 * This class was written by Abdul Hannan Ayubi.
 * Tarih : 05/10/2020
 */
#include"Encryption.h"
#include<iostream>
#include<string>
using namespace std;
/// <encrypy>
/// This function is using for encrypting the given int num for four digits
/// </encrypt>
/// <@param name="num"></@param>
/// <returns int ></returns int>
int Encryption::encrypt(int num) {
	string temp = to_string(num) , Number = "";
	int arr[4];
	
	unsigned int res, number = 0;
	for (int i = 0; i < temp.size(); i++) {
		arr[i] = ((temp[i] - '0' + 7) % 10);
	}
	for (int i = 0; i < temp.size(); i++) {
		Number.push_back(arr[i] + '0');
	}
	char first = Number[0];
	char third = Number[2];
	Number[0] = third;
	Number[2] = first;
	first = Number[1];
	third = Number[3];
	Number[1] = third;
	Number[3] = first;
	res = stod(Number);
	return res;
}
/// <decrypt>
/// this function is using for decrypt the given encrypted function
/// </decrypt>
/// <@param name="num"></@param>
/// <returns int ></returns int>
int Encryption::decrypt(int num) {
	string temp = to_string(num), Number = ""; /// changing int to string 
	unsigned int res; 
	char first = temp[0];
	char third = temp[2];
	temp[0] = third;
	temp[2] = first;
	first = temp[1];
	third = temp[3];
	temp[1] = third;
	temp[3] = first;
	int arr[4];
	for (int i = 0; i < 4; i++) {
		arr[i] = ((temp[i] - '0' + 3) % 10); /// using forumla for the decrypting the code
	}
	for (int i = 0; i < temp.size(); i++) {
		Number.push_back(arr[i] + '0');
	}
	res = stod(Number);
	return res;
}
