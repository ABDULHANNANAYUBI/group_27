﻿/**
 * This class was written by Serdar Demirtas.
 * Tarih: 06/01/2021
 */

#include "PioneerRobotAPI.h"
#include "SonarSensor.h"

#define SIZE 16

const float angles[SIZE] = {90,50,30,10,-10,-30,-50,-90,-90,-130,-150,-170,170,150,130,90};
bool tempFlag = false;

void SonarSensor::setRobot(PioneerRobotAPI* newRobotAPI) {
	this->robotAPI = new PioneerRobotAPI();
	this->robotAPI = newRobotAPI;
}

/// <SonarSensory>
/// Default Constructor
/// </SonarSensory>
SonarSensor::SonarSensor() {
	ranges = new float[SIZE];
	for (int i = 0; i < SIZE; i++) {
		ranges[i] = 0.0;
	}
}

/// <SonarSensory>
///	Parameterized Constructor
/// </SonarSensory>
/// <param name="ranges"></param>
SonarSensor::SonarSensor(float*ranges) {
	if (tempFlag == false) {
		this->ranges = new float[SIZE];
		for (int i = 0; i < SIZE; i++) {
			this->ranges[i] = ranges[i];
		}
	}
	else
		return;
}


/// <SonarSensor>
/// Copy Constructor
/// </SonarSensor>
/// <param name="Sonar"></param>
SonarSensor::SonarSensor(const SonarSensor& Sonar) {
	ranges = new float[SIZE];

	for (int i = 0; i < SIZE; i++) {
		ranges[i] = Sonar.ranges[i];
	}
}

/// <getRange>
/// Returns distance information of the sensor with index.
/// </getRange>
/// <param name="index"></param>
/// <returns></returns>
float SonarSensor::getRange(int index) {
	return ranges[index];
}

/// <getMax>
/// Returns the maximum of the distance values. The index of data with this distance returns the variable in parentheses
/// </getMax>
/// <param name="index"></param>
/// <returns></returns>
float SonarSensor::getMax(int& index) {
	int max = ranges[0];
	int i = 0;
	for (; i < SIZE; i++) {
		if (max < ranges[i]) {
			max = ranges[i];
		}
	}
	
	for (int j = 0; j < SIZE; j++) {
		if (ranges[j] == max) {
			index = j;
			break;
		}
	}

	return max;
}

/// <getMin>
/// Returns the minimum of the distance values. The index of data with this distance returns the variable in parentheses
/// </getMin>
/// <param name="index"></param>
/// <returns></returns>
float SonarSensor::getMin(int& index) {
	int min = ranges[0];
	int i = 0;
	for (; i < SIZE; i++) {
		if (min > ranges[i]) {
			min = ranges[i];
		}
	}

	for (int j = 0; j < SIZE; j++) {
		if (ranges[j] == min) {
			index = j;
			break;
		}
	}

	return min;
}

/// <updateSensor>
/// Uploads the robot's current sensor distance values ​​to the ranges array
/// </updateSensor>
/// <param name="ranges"></param>
void SonarSensor::updateSensor(float*ranges) {
	robotAPI->getSonarRange(ranges);
	for (int i = 0; i < SIZE; i++) {
		this->ranges[i] = ranges[i];
	}
}

/// <operator[]>
/// Returns the sensor value given the index. Implements the function similar to getRange(i)
/// </operator[]>
/// <param name="i"></param>
/// <returns></returns>
float SonarSensor::operator[](int i) {
	return this->ranges[i];
}

/// <getAngle>
/// Returns the angle value of the sensor with the index given
/// </getAngle>
/// <param name="index"></param>
/// <returns></returns>
float SonarSensor::getAngle(int index) {
	return angles[index];
}
float SonarSensor:: getClosestRange(float startAngle, float endAngle, float&angle) {
	float min = ranges[0];
	for (int i = startAngle; i < endAngle; i++) {
		if (min > ranges[i]) {
			min = ranges[i];
		}
	}

	for (int i = startAngle; i < endAngle; i++) {
		if (min == ranges[i]) {
			angle = i;
			break;
		}
	}
	return min;
}
