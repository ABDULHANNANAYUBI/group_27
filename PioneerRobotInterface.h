﻿/**
* This class Implemented by Abdul Hannan Ayubi & Furkan Taşkın
* This class is for the Robot type objects..
* Class implementation purpose is to use a virtual classes of this function...
* Date : 13/01/2021
*/

#ifndef _PIONEERROBOTINTERFACE_
#define _PIONEERROBOTINTERFACE_
#include "RobotInterface.h"
#include"PioneerRobotAPI.h"
#include"RangeSensor.h"
class PioneerRobotInterface : public RobotInterface {
private:
	PioneerRobotAPI* RobotAPI;
	float ranges[1800];
public:
	PioneerRobotInterface();
	~PioneerRobotInterface();
	void turnLeft();
	void turnRight();
	void forward(float);
	void print();
	void backword(float);
	void setPose(Pose*);
	Pose getPose();
	void stopTurn();
	void stopMove();
	void updateSensors(RangeSensor*);
	void setState(int);
	int getState();
	PioneerRobotAPI* getRobot() { return this->RobotAPI; }
};




#endif // !_PIONEERROBOTINTERFACE_