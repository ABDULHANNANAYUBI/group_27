﻿/**
 * This class was written by Abdul Hannan Ayubi.
 * Tarih : 05/10/2020
 */
#include "Record.h"
#include<string>
#include<iostream>
#include<fstream>
using namespace std;
/// <Record>
/// Record is default constructor for initilizing the @param name="fileName" @param name = "input"
/// </End Record>
Record::Record() {
	this->fileName = this->input = "";
}
/// <Record>
/// Record is parametrized constructor for the file name of the input and output txt file
/// </End Record>
/// <@param name="fileName"></param>
Record::Record(string fileName) {
	this->fileName = fileName;
}
/// <Record>
/// initializing the param by getting the input from users
/// </End Record>
/// <@param name="fileName"></param>
/// <@param name="input"></param>
Record::Record(string fileName, string input) {
	this->fileName = fileName;
	this->input = input;
}
/// <SetFileName>
/// Setting the file name for the file 
/// </End SetFileName>
/// <@param name="fileName"></param>
void Record::setFileName(string fileName) {
	this->fileName = fileName;
}
/// <OpenFile>
/// Opening the file which is taken the name by the user..
/// </End OpenFile>
/// <returns boolean></returns>
bool Record::openFile() {
	file.open(fileName, ios::in);
	if (!file) {
		cout << "File doesn't found...!\n";
		exit(0); /// For wrong input file names
	}
	else
		cout << "File Opened Successfully...!\n";
}
/// <CloseFile>
/// closeFile is using purpose is for the closing the open file and prevent
/// Private parameter
/// @param file
/// </closeFile>
/// <returns boolean></returns boolean>
bool Record::closeFile() {
	file.close(); /// Closing the file if the file is open
	return true;
}
/// <readLine>
/// Readline function is reading a string from the file and return the readed file 
/// </readLine>
/// <returns string ></returns>
string Record::readLine() {
	string realString;
	while (!file.eof()) {
		string temp = "";
		getline(file, temp);
		temp.push_back(' ');
		realString += temp;
	}
	return realString;
}
/// <WriteLine Function>
/// 
/// </summary>
/// <param name="str"></param>
/// <returns></returns>
bool Record::writeLine(string str) {
	fstream writefile;
	writefile.open("output.txt", ios::out);
	writefile << str << endl;
	if(closeFile())
		return true;
}
/// <Operator>
/// This function is using for operator overloading function for output
/// </Operator>
/// <@param name="out"></param>
/// <@param name="record"></param>
/// <returns output></returns output>

ostream& operator << (ostream& out, Record& record) {
	out << record.readLine();
	return out;
}

/// <operator overloading>
/// this is using for getting input and I am calling WriteLine...
/// </operator overloading>
/// <@param name="in"></param>
/// <@param name="record"></param>
/// <returns operator ></returns operator>
istream& operator >> (istream& in, Record& record) {
	in >> record.input;
	record.writeLine(record.input);
	return in;
}