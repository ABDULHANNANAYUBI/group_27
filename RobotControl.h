/**
 * This class was written by Osman �aglar.
 * This class controls the robot's movements and tells the robot the function it must do.
 * Date: 04/01/2021.
 */
#ifndef ROBOT_CONTROL_H
#define ROBOT_CONTROL_H

#include"Pose.h"
#include"PioneerRobotAPI.h"
#include"PioneerRobotInterface.h"
#include"Path.h"
#include"Record.h"
#include"RobotOperator.h"


class RobotControl {
private:
	PioneerRobotInterface* pioneerRobotInterface;
	Path* path;
	Record* record;
	RobotOperator* robotOperator;
	RangeSensor* rangesensor;
	bool flag;
	float ranges[1800];
	/*Pose* pose; /// This member variable helps the robot to store its current position.
	PioneerRobotAPI* robot; /// This member variable helps to use commands in the PioneerRobotAPI class in this class.
	int state; /// This member variable stores the current connection state of the robot. Stores 1 if the robot is connected, 0 if it is not.*/
public:
	RobotControl(); /// Allows the creation of members from the PioneerRobotAPI and Pose classes. Provides the connection of the robot.
	~RobotControl(); /// It plays a role in deleting created objects and disconnecting the robot.
	void turnLeft(); /// Performs the robot's continuous rotation to the left.
	void turnRight(); /// Performs the robot's continuous rotation to the right.
	void forward(float); /// @param speed required motion speed to forward for the robot (millimeter/seconds).
	void print(); /// Prints the robot's current location information and connection status on the screen.
	void backward(float); /// @param speed required motion speed to backward for the robot (millimeter/seconds).
	Pose getPose(); /// @return the robot's current position from the Pose object.
	void setPose(Pose*); /// @param to set a new location to the robot.
	void stopTurn(); /// Terminates the robot's rotational motion.
	void stopMove(); /// Stops the robot's motion.
	PioneerRobotAPI* getRobot() { return this->pioneerRobotInterface->getRobot(); }
	void login();
	bool addToPath();
	bool clearPath();
	bool recordPathToFile();
	bool openAccess(int);
	bool closeAccess();
	bool getFlag() { return this->flag; }
	void updateSensors(RangeSensor*);
	void createRobotControl() { pioneerRobotInterface = new PioneerRobotInterface(); }
	RangeSensor* getRangeSensor() { return this->rangesensor; }
};

#endif // !ROBOT_CONTROL_H