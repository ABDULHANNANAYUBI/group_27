﻿/**
 * This class was written by Serdar Demirtas.
 * Tarih: 06/01/2021
 */

#include "PioneerRobotAPI.h"
#include "RangeSensor.h"


class SonarSensor: public RangeSensor {
private:
	float* ranges = new float[16];
	PioneerRobotAPI* robotAPI;
public:
	SonarSensor();
	SonarSensor(float*);
	SonarSensor(const SonarSensor&);
	float getRange(int);
	float getMax(int&);
	float getMin(int&);
	void updateSensor(float*);
	float getClosestRange(float, float, float&);
	float operator[](int);
	float getAngle(int);
	void setRobot(PioneerRobotAPI*);
};
