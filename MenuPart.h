#ifndef _MENU_
#define _MENU_
#include"RobotControl.h"
#include"LaserSensor.h"
#include"SonarSensor.h"
class Menu {
private:
	RobotControl* robotControl;
	RangeSensor* rangesensor;
	Path* path;
	float ranges[1800];
	float sonarRanges[1800];

	int key;
public:
	Menu();
	bool login();
	bool mainMenu();
	void connectionMenu();
	void motionMenu();
	void sensorMenu();
	void positionMenu();
	void record();
};


#endif // !_MENU_

