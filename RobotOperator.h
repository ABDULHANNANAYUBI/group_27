/**
 * This class was written by Abdul Hannan Ayubi.
 * Tarih : 05/10/2020
 */
#pragma once
#include<string>
#include<iostream>
#include "Encryption.h"

using namespace std;

class RobotOperator : public Encryption{ /// RobotOperator classH class and getting inherit form teh Encryption class
private:
	string name; /// String Parameter
	string surname; /// String Parameter
	unsigned int accessCode; /// access code with unsigned
	bool accessState; /// Access State
public:
	RobotOperator();
	RobotOperator(string, string); ///Getting two parametrized Function
	RobotOperator(string, string, int); /// Getting three Parametrized Function
	RobotOperator(unsigned int); /// Getting only code which is the pdf given
	int encryptCode(int); /// using encryptcode function from encryption
	int decryptCode(int); /// using decryptcode fucntion from decryption function from inherited class
	bool checkAccessCode(int); /// Cheak accessCode of the parametrized Code
	void print(); /// Printing the result
};