/**
 * This class was written by Furkan Taskin.
 * 07.01.2021
 */

#ifndef PATH_H
#define PATH_H
#include "Node.h"
#include "Pose.h"
#include <iostream>

using namespace std;

class Path
{
public:
	Path(); /// default constructor
	void addPos(Pose); /// adding positions.
	void print(); /// printing positons properly.
	Pose operator[](int); /// returns the position in the given index.
	Pose getPos(int); /// returns the position in the given index.
	bool removePos(int); /// Deletes the position in the given index from the list.
	bool insertPos(int, Pose); /// Adds a position after the given index.
	friend ostream& operator<<(ostream&, Path&); /// print performs the task that the function does.
	friend istream& operator>> (istream&, Pose*); /// Adds the location entered from the keyboard to the end of the list.
	~Path(); /// default destructor
	Node* getHead() { return this->head; }
	string getPoseAsString();

private:
	Node* tail;
	Node* head;
	int number;
};

#endif // !PATH_H
