/**
 * This class was written by Furkan Taskin and Osman �aglar.
 * This class checks the robot's location information.
 * 03.01.2021
 */


#ifndef POSE_H
#define POSE_H

#include<iostream>

using namespace std;

class Pose {
private:
	float x;	/// The x coordinate of the robot,
	float y;	/// the y coordinate of the robot
	float th;	/// and stores the current angle of the robot.
public:
	Pose(); /// Default constructor: Assigns 0 to x, y and th values.
	Pose(float, float, float); /// Parameter constructor: Assigns user input to x, y and th values.
	~Pose() {}
	float getX() const; /// Set and Get functions: Returns or sets the desired values.
	void setX(float);	
	float getY() const;
	void setY(float);
	float getTh() const;
	void setTh(float);
	bool operator==(const Pose&); /// @return true if the two pose is equal, otherwise returns false. @param gets Pose reference.
	Pose operator+(const Pose&); /// @return sum of two positions. @param gets Pose reference.
	Pose operator-(const Pose&); /// @return the difference of the two positions. @param gets Pose reference.
	Pose& operator+=(const Pose&); /// @return allows cascading. @param gets Pose reference.
	Pose& operator-=(const Pose&); /// @return allows cascading. @param gets Pose reference.
	bool operator<(const Pose&); /// @return true if the position is less than the position entered as a parameter.
	void getPose(float&, float&, float&) const; /// Set and Get functions: Returns or sets the desired values.
	void setPose(float, float, float);
	float findDistanceTo(Pose); /// @return the distance between the position entered as a parameter and the current position.
	float findAngleTo(Pose); /// @return the angle between the position entered as a parameter and the current position.
	friend ostream& operator<<(ostream&, const Pose&); /// Prints x, y and th in a neat format on screen.
	friend istream& operator>> (istream&, Pose&); /// Gets x, y and th values from the user.
};

#endif // !POSE_H
