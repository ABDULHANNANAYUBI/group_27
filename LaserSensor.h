/**
 * This class was written by Serdar Demirtas
 * Tarih: 06/01/2021
 */
#include "PioneerRobotAPI.h"
#include "RangeSensor.h"
#include<iostream>


using namespace std;

class LaserSensor: public RangeSensor  {
private:
	float* ranges = new float[181];
	PioneerRobotAPI* robotAPI;
public:
	LaserSensor();
	LaserSensor(float*);
	LaserSensor(const LaserSensor&);
	float getRange(int);
	float getMax(int&);
	float getMin(int&);
	void updateSensor(float*);
	float operator[](int);
	float getAngle(int);
	float getClosestRange(float,float,float&);
	void setRobot(PioneerRobotAPI*);
};
