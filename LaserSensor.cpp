﻿/**
 * This class was written by Serdar Demirtas.
 * Tarih: 06/01/2021
 */

#include "PioneerRobotAPI.h"
#include "LaserSensor.h"

#define SIZE 181

float Angles[SIZE] = { 0.0 };
bool flag = false;

void LaserSensor::setRobot(PioneerRobotAPI* newRobotAPI) {
	this->robotAPI = new PioneerRobotAPI();
	this->robotAPI = newRobotAPI;
}


/// <SetAngle>
/// Setting values of Angles array
/// </SetAngle>
void SetAngle() {
	for (int i = 0; i < SIZE; i++) {
		Angles[i] = i;
	}
}

/// <LaserSensor>
/// Default Constructor
/// </LaserSensor>
LaserSensor::LaserSensor() {
	
	SetAngle();
	
	for (int i = 0; i < SIZE; i++) {
		ranges[i] = 0.0;
	}
}

/// <LaserSensor>
/// Parameterized Constructor
/// </LaserSensor>
/// <param name="ranges"></param>
LaserSensor::LaserSensor(float* ranges) {
	if (flag == false) {
		SetAngle();

		for (int i = 0; i < SIZE; i++) {
			this->ranges[i] = ranges[i];
		}
		flag = true;
	}
	else
		return;
}

/// <LaserSensor>
/// Copy Constructor
/// </LaserSensor>
/// <param name="Laser"></param>
LaserSensor::LaserSensor(const LaserSensor& Laser) {
	
	SetAngle();
	
	ranges = new float[SIZE];
	for (int i = 0; i < SIZE; i++) {
		ranges[i] = Laser.ranges[i];
	}
}


/// <getRange>
/// Returns distance information of sensor with index i
/// </getRange>
/// <param name="index"></param>
/// <returns></returns>
float LaserSensor::getRange(int index) {
	return ranges[index];
}

/// <getMax>
/// Returns the maximum of the distance values. The index of data with this distance returns the variable in parentheses
/// </getMax>
/// <param name="index"></param>
/// <returns></returns>
float LaserSensor::getMax(int& index) {
	int max = ranges[0];
	int i = 0;
	for (; i < SIZE; i++) {
		if (max < ranges[i]) {
			max = ranges[i];
		}
	}

	for (int j = 0; j < SIZE; j++) {
		if (ranges[j] == max) {
			index = j;
			break;
		}
	}

	return max;
}

/// <getMin>
/// Returns the minimum of the distance values. The index of data with this distance returns the variable in parentheses
/// </getMin>
/// <param name="index"></param>
/// <returns></returns>
float LaserSensor::getMin(int& index) {
	int min = ranges[0];
	int i = 0;
	for (; i < SIZE; i++) {
		if (min > ranges[i]) {
			min = ranges[i];
		}
	}

	for (int j = 0; j < SIZE; j++) {
		if (ranges[j] == min) {
			index = j;
			break;
		}
	}

	return min;
}

/// <updateSensor>
/// Uploads the robot's current sensor distance values ​​to the ranges array
/// </updateSensor>
/// <param name="ranges"></param>
void LaserSensor::updateSensor(float* ranges) {
	robotAPI->getLaserRange(ranges);
	for (int i = 0; i < SIZE; i++) {
		this->ranges[i] = ranges[i];
	}
}

/// <operator[]>
/// Returns the sensor value given the index. Implements the function similar to getRange (i)
/// </operator[]>
/// <param name="i"></param>
/// <returns></returns>
float LaserSensor::operator[](int i) {
	return this->ranges[i];
}

/// <getAngle>
/// Returns the angle value of the sensor with the index given
/// </getAngle>
/// <param name="index"></param>
/// <returns></returns>
float LaserSensor::getAngle(int index) {
	return Angles[index];
}

/// <getClosestRange>
/// Between startAngle and endAngle angles Returns the angle of the smallest distance on angle and the distance with return.
/// </getClosestRange>
/// <param name="startAngle"></param>
/// <param name="endAngle"></param>
/// <param name="angle"></param>
/// <returns></returns>
float LaserSensor::getClosestRange(float startAngle,float endAngle,float& angle) {
	float min = ranges[0];
	for (int i = startAngle; i < endAngle; i++) {
		if (min > ranges[i]) {
			min = ranges[i];
		}
	}

	for (int i = startAngle; i < endAngle; i++) {
		if (min == ranges[i]) {
			angle = i;
			break;
		}
	}
	return min;
}