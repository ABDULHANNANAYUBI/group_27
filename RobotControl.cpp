/**
 * This class was written by Osman �aglar.
 * This class controls the robot's movements and tells the robot the function it must do.
 * Date: 04/01/2021.
 */

#include<iostream>
#include<iomanip>
#include"RobotControl.h"

using namespace std;

void getAccept(string& name, string& surName, int& accessCode) {
	string Name, sunname;
	int accesscode;
	cout << "Enter your name: ";
	getline(cin, Name);

	cout << "Enter your surname: ";
	getline(cin, sunname);

	cout << "Enter your access code: ";
	cin >> accesscode;
	name = Name, surName = sunname;
	accessCode = accesscode;
}

RobotControl::RobotControl() {
	
	path = new Path();
	record = new Record();
	//record->openFile();
	flag = false;
	memset(ranges, 0, sizeof(ranges));
	
}
RobotControl::~RobotControl() {
	pioneerRobotInterface->~PioneerRobotInterface();
	
}
void RobotControl::turnLeft() {
	pioneerRobotInterface->turnLeft();
	
}
void RobotControl::turnRight() {
	pioneerRobotInterface->turnRight();
	
}
void RobotControl::forward(float speed) {
	pioneerRobotInterface->forward(speed);
	
}
void RobotControl::print() {
	pioneerRobotInterface->print();
}
void RobotControl::backward(float speed) {
	pioneerRobotInterface->backword(speed);
	
}
Pose RobotControl::getPose() {
	return pioneerRobotInterface->getPose();
	
}
void RobotControl::setPose(Pose* pose) {
	pioneerRobotInterface->setPose(pose);
}
void RobotControl::stopTurn() {
	pioneerRobotInterface->stopTurn();


	
}
void RobotControl::stopMove() {
	pioneerRobotInterface->stopMove();
	
}
void RobotControl::login() {
	string name, surname;
	int accessCode;
	getAccept(name, surname, accessCode);
	robotOperator = new RobotOperator(name, surname, accessCode);
	robotOperator->print();
}
bool RobotControl::addToPath() {
	path->addPos(pioneerRobotInterface->getPose());
	return true;
}
bool RobotControl::clearPath() {
	path->~Path();
	return true;
}
bool RobotControl::recordPathToFile() {	
	record->writeLine(path->getPoseAsString());
	return true;
}
bool RobotControl::openAccess(int code) {
	flag = robotOperator->checkAccessCode(code);
	return flag;
}
bool RobotControl::closeAccess() {
	flag = false;
	return flag;
}

void RobotControl::updateSensors(RangeSensor* rangeSensor) {
	this->rangesensor = rangesensor;
	this->rangesensor->updateSensor(this->ranges);
}