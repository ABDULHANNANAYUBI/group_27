#include"MenuPart.h"

Menu::Menu() :key(0) {
	robotControl = new RobotControl();
	path = new Path();
	memset(ranges, 0, sizeof(ranges));
}

bool Menu::login() {
	int accesscode;
	robotControl->login();
	cout << "Enter Access Code : ";
	cin >> accesscode;
	if (robotControl->openAccess(accesscode)) {
		cout << "Login Successful" << endl;
		return true;
	}
	else {
		cout << "Acceess Code is Wrong " << endl;
		return false;
	}
}

bool Menu::mainMenu() {
	static bool log = false;
	if (!log) {
		log = login();
	}
	if (log) {
		cout << "\n<Main Menu>\n"
			<< "1. Connection\n"
			<< "2. Motion\n"
			<< "3. Sensor\n"
			<< "4. Position\n"
			<< "5. Record\n"
			<< "6. Quit\n";

		cout << "Choose one: ";
		cin >> key;
		if (key == 6) {
			exit(0);
		}
		else if (key == 1 && robotControl->getFlag())
			connectionMenu();
		else if (key == 2 && robotControl->getFlag())
			motionMenu();
		else if (key == 3 && robotControl->getFlag())
			sensorMenu();
		else if (key == 4 && robotControl->getFlag())
			positionMenu();
		else if (key == 5 && robotControl->getFlag())
			record();
		mainMenu();
	}
	else {
		cout << "Login unsuccessful" << endl;
		exit(0);
	}
	
}

void Menu::connectionMenu() {
	cout << "\n<Connection Menu>\n"
		<< "1. Connect Robot\n"
		<< "2. Disconnect Robot \n"
		<< "3. Back\n";
	cout << "Choose one: ";
	cin >> key;

	if (key == 1) {
		robotControl->createRobotControl();
		connectionMenu();
	}
	else if (key == 2) {
		robotControl->~RobotControl();
		connectionMenu();
	}
	else if (key == 3)
		return;

	return;
}

void Menu::motionMenu() {
	int speed, movementTime , distance;
	cout << "\n<Motion Menu>\n"
		<< "1. Move robot\n"
		<< "2. Turn left\n"
		<< "3. Turn Right\n"
		<< "4. Forward \n"
		<< "5. Backward \n"
		<< "6. Stop Turn \n"
		<< "7. Stop Move \n"
		<< "8. Move Forward with Distance \n"
		<< "9. Move Backward with Distance \n"
		<< "10. Back \n";
	cout << "Choose one: ";
	cin >> key;

	if (key == 1) {
		cout << "Enter speed: ";
		cin >> speed;

		robotControl->forward(speed);
	}
	else if (key == 2) {
		robotControl->turnLeft();
	}
	else if (key == 3) {
		robotControl->turnRight();
	}
	else if (key == 4) {
		cout << "Enter speed and movement time: ";
		cin >> speed >> movementTime;

		robotControl->forward(speed);
		Sleep(movementTime);
	}
	else if (key == 5) {
		cout << "Enter speed and movement time: ";
		cin >> speed >> movementTime;

		robotControl->backward(speed);
		Sleep(movementTime);
	}
	else if (key == 6) {
		robotControl->stopTurn();
	}
	else if (key == 7) {
		robotControl->stopMove();
	}
	else if (key == 8) {
		cout << "Enter speed and distance: ";
		cin >> speed >> distance;

		robotControl->forward(speed);
		Sleep((float)distance / speed);
	}
	else if (key == 9) {
		cout << "Enter speed and distance: ";
		cin >> speed >> distance;

		robotControl->backward(speed);
		Sleep(distance / speed);
		robotControl->stopMove();
		robotControl->print();
	}
	else if (key == 10)
		return;

	return;
}

void Menu::sensorMenu() {

	cout << "\n<Sensor Menu>\n"
		<< "1. Get Laser Ranges\n"
		<< "2. Get Maximum Laser Range and Index\n"
		<< "3. Get Minimum Laser Range and Index\n"
		<< "4. Get Sonar Ranges\n"
		<< "5. Get Maximum Sonar Range and Index\n"
		<< "6. Get Minimum Sonar Range and Index\n"
		<< "7. Back \n";
	cout << "Choose one: ";
	cin >> key;

	if (key == 1) {
		rangesensor = new LaserSensor();
		rangesensor->setRobot(robotControl->getRobot());
		rangesensor->updateSensor(ranges);
		cout << "Laser Ranges: [";
		for (int i = 0; i < 181; i++) {
			cout << ranges[i] << ", ";
		}
		cout << "]\n";
	}
	else if (key == 2) {
		int maxIndex;
		rangesensor = new LaserSensor(ranges);
		rangesensor->setRobot(robotControl->getRobot());
		rangesensor->updateSensor(ranges);
		cout << "Maximum Laser Range: " << rangesensor->getMax(maxIndex) << endl;
		cout << "Maximum Laser Index: " << maxIndex << endl;
	}
	else if (key == 3) {
		int minIndex;
		rangesensor = new LaserSensor(ranges);
		rangesensor->setRobot(robotControl->getRobot());
		rangesensor->updateSensor(ranges);
		cout << "Minimum Laser Range: " << rangesensor->getMin(minIndex) << endl;
		cout << "Minimum Laser Index: " << minIndex << endl;
	}
	else if (key == 4) {
		rangesensor = new SonarSensor(sonarRanges);
		rangesensor->setRobot(robotControl->getRobot());
		rangesensor->updateSensor(sonarRanges);
		cout << "Sonar Ranges: [";
		for (int i = 0; i < 16; i++) {
			cout << sonarRanges[i] << ", ";
		}
		cout << "]\n";
	}
	else if (key == 5) {
		int maxIndex;
		rangesensor = new SonarSensor(sonarRanges);
		rangesensor->setRobot(robotControl->getRobot());
		rangesensor->updateSensor(sonarRanges);
		cout << "Maximum Sonar Range: " << rangesensor->getMax(maxIndex) << endl;
		cout << "Maximum Sonar Index: " << maxIndex << endl;
	}
	else if (key == 6) {
		int minIndex;
		rangesensor = new SonarSensor(sonarRanges);
		rangesensor->setRobot(robotControl->getRobot());
		rangesensor->updateSensor(sonarRanges);
		cout << "Minimum Sonar Range: " << rangesensor->getMin(minIndex) << endl;
		cout << "Minimum Sonar Index: " << minIndex << endl;
	}
	else if (key == 7) {
		return;
	}
	return;
}

void Menu::positionMenu() {
	cout << "\n<Position Menu>\n"
		<< "1. Get current position\n"
		<< "2. Add new position\n"
		<< "3. Insert new position\n"
		<< "4. Remove a position\n"
		<< "5. Print all positions\n"
		<< "6. Back \n";
	cout << "Choose one: ";
	cin >> key;

	if (key == 1) {
		robotControl->print();
	}
	else if (key == 2) {
		path->addPos(robotControl->getPose());
		this->robotControl->addToPath();
	}
	else if (key == 3) {
		int index;
		cout << "Enter a index to insert a new position: ";
		cin >> index;
		this->robotControl->addToPath();
		path->insertPos(index, robotControl->getPose());
	}
	else if (key == 4) {
		if (!path->getHead()) {
			cout << "There is no position to delete!\n";
			return;
		}
		else {
			int index;
			cout << "Enter a index to remove a position: ";
			cin >> index;
			path->removePos(index);
		}

	}
	else if (key == 5) {
		if (!path->getHead()) {
			cout << "There is no position to print!\n";
			return;
		}
		else
			path->print();
	}
	else if (key == 6)
		return;

	return;

}

void Menu::record() {
	cout << "\n<Record Menu>\n"
		<< "1. Save Record Position to file \n"
		<< "2. Back \n";
	cout << "Choose one: ";
	cin >> key;
	if (key == 1) {
		this->robotControl->recordPathToFile();
	}
	else if (key == 2) {
		return;
	}
	return;

}