/**
 * This class was written by Furkan Taskin and Osman �aglar.
 * This class checks the robot's location information.
 * 03.01.2021
 */


#include<iostream>
#include<math.h>
#include"Pose.h"

using namespace std;

Pose::Pose() :x(0), y(0), th(0) {}
Pose::Pose(float x, float y, float th) :x(x), y(y), th(th) {}
float Pose::getX() const {
	return x;
}
void Pose::setX(float x) {
	this->x = x;
}
float Pose::getY() const {
	return y;
}
void Pose::setY(float y) {
	this->y = y;
}
float Pose::getTh() const {
	return th;
}
void Pose::setTh(float th) {
	this->th = th;
}
bool Pose::operator==(const Pose& pose) {
	return (this->x == pose.x && this->y == pose.y && this->th == pose.th);
}
Pose Pose::operator+(const Pose& pose) {
	Pose result;
	if (this->getTh() == pose.getTh()) {
		result.setX(this->x + pose.getX());
		result.setY(this->y + pose.getY());
		result.setTh(this->getTh() + pose.getTh());
	}
	else {
		result.setX(pose.getX());
		result.setY(pose.getY());
		result.setTh(pose.getTh());
	}
	
	return result;
}
Pose Pose::operator-(const Pose& pose) {
	Pose result;
	if (this->getTh() == pose.getTh()) {
		result.setX(this->x - pose.getX());
		result.setY(this->y - pose.getY());
		result.setTh(this->getTh() - pose.getTh());
	}
	else {
		result.setX(pose.getX());
		result.setY(pose.getY());
		result.setTh(pose.getTh());
	}
	return result;
}
Pose& Pose::operator+=(const Pose& pose) {
	if (this->getTh() == pose.getTh()) {
		this->x += pose.getX();
		this->y += pose.getY();
		this->th += pose.getTh();
	}
	return (*this);
}
Pose& Pose::operator-=(const Pose& pose) {
	if (this->getTh() == pose.getTh()) {
		this->x -= pose.getX();
		this->y -= pose.getY();
		this->th -= pose.getTh();
	}
	return (*this);
}
bool Pose::operator<(const Pose& pose) {
	return (this->x < pose.getX()) && (this->y < pose.getY());
}
void Pose::getPose(float& x, float& y, float& th) const {
	x = this->x;
	y = this->y;
	th = this->th;
}
void Pose::setPose(float x, float y, float th) {
	this->x = x;
	this->y = y;
	this->th = th;
}
float Pose::findDistanceTo(Pose x) {
	return sqrt(pow((x.getX() - this->x, 2) + pow(x.getY() - this->y, 2), 2));
}
float Pose::findAngleTo(Pose) {
	// Anlayamad�k??
	return this->getTh();
}
ostream& operator<<(ostream& out, const Pose& p) {
	out << "(" << p.getX() << ", " << p.getY() << ", " << p.getTh() << ")";
	return out;
}
istream& operator>>(istream& in, Pose& p) {
	float TempX, TempY, TempTh;
	in >> TempX >> TempY >> TempTh;
	p.setX(TempX), p.setY(TempY), p.setTh(TempTh);

	return in;
}