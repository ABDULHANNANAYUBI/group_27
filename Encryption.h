/**
 * This class was written by Abdul Hannan Ayubi.
 * Tarih : 05/10/2020
 */

#pragma once
class Encryption { /// Start Class Encryption
public:
	int encrypt(int);/// function encrypt for the encrypting code
	int decrypt(int);/// function decrypt for the decrypt code
};