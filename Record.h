/**
 * This class was written by Abdul Hannan Ayubi.
 * Tarih : 05/10/2020
 */
#ifndef _RECORD_
#define _RECORD_
#include<fstream>
#include<string>
#include<iostream>

using namespace std;
/// <Record Class>
/// @param : fileName
/// @param : input
/// @param : file
/// </Record Class>
class Record {
private:
	string fileName; /// Getting the file name
	string input; /// Getting input from the user 
	fstream file; /// using input and output class for the file
public:
	Record(); /// Deafault constructor
	Record(string); /// Parametrized Constructor
	Record(std::string, string); /// Prametrized Constructor for getting file name as well as input as a string
	bool openFile(); /// Openining the file which is first input file from the user
	bool closeFile(); /// Closing the opened file 
	void setFileName(string); /// Setter Method (For Setting the file name which is entered by user)
	string readLine(); /// Reading line of input file 
	bool writeLine(string); /// Writing a text or output for the file
	friend ostream& operator <<(ostream&, Record&); /// streaming the input and output for the file
	friend istream& operator >>(istream&, Record&); /// Input the enterd string file from the user
};

#endif // RECORD