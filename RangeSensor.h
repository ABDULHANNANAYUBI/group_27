#pragma once

#include <iostream>
#include "PioneerRobotAPI.h"

using namespace std;

class RangeSensor {
private:
	float *ranges;
	PioneerRobotAPI* RobotAPI;
public:
	virtual float getRange(int) = 0;
	virtual float getMax(int&) = 0;
	virtual float getMin(int&) = 0;
	virtual void updateSensor(float*) = 0;
	virtual float operator[](int) = 0;
	virtual float getAngle(int) = 0;
	virtual float getClosestRange(float, float, float&) = 0;
	virtual void setRobot(PioneerRobotAPI*) = 0;
};


